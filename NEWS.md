<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the parse-stages project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.4] - 2024-12-29

### Semi-incompatible changes

- Python test suite:
    - drop support for Python 3.7, 3.8, and 3.9

### Additions

- Add a top-level Tox definition for running `reuse` to check the SPDX
  copyright and license tags
- Python test suite:
    - add a `ruff` Tox environment
    - add tags to the test suite definitions for use with
      [tox-stages](https://devel.ringlet.net/devel/test-stages/)
    - add a manually-invoked `pyupgrade` environment
    - add Nix expressions for running the tests with different Python versions
    - list Python 3.11, 3.12, 3.13, and 3.14 as supported versions
    - add Debian bookworm, Ubuntu noble, and Rocky Linux 9 as supported
      Docker container images

### Other changes

- Switch to yearless copyright notices
- Switch to SPDX copyright and license tags
- Convert this changelog file to the "Keep a Changelog" format
- Python test suite:
    - convert the `tox.ini` file to the Tox 4.x format
    - move the requirement files into a `requirements/` subdirectory
    - use Ruff for source code formatting
    - various minor refactoring suggested by Ruff
    - use Mypy 1.7 or higher
    - use `logging` instead of `cfg-diag` for diagnostic messages
    - support operation without `utf8-locale` installed; try to run
      `u8loc` anyway and, if that fails, run `locale -a` and look for
      the `C.UTF-8` or `C.utf8` locale names
    - look for `sshd` in the current search path first
    - drop the `pep8` and `pylint` test environments
    - switch to `hatchling` for the PEP517 build
    - simplify some constructs with Python 3.8 and 3.10 features
    - use Debian bookworm, not bullseye, as the default Docker image
    - drop Debian buster and bullseye, Ubuntu focal, AlmaLinux, and
      CentOS as supported Docker container images
- Rust test generation:
    - various minor refactoring suggested by Clippy
    - bump MSRV to 1.58 for the inline format arguments
    - use the `camino` library for UTF-8 paths
    - use `tracing` instead of `config-diag` for diagnostic messages

## [0.2.3] - 2022-11-03

### Other changes

- Python test suite:
  - use version 1.x of the utf8-locale library with no changes
  - adapt the test programs to the new API of version 0.4 of the cfg-diag
    library, hopefully for the last time
- Rust test generation tool:
  - use the anyhow library instead of the expect-exit one for terminating
    the program's operation if an error should occur
  - use version 4.x of the clap library with no changes
  - drop some overrides for the Clippy diagnostic tool, they were not
    even triggered by our source code

## [0.2.2] - 2022-09-01

### Additions

- Add the `generate_tests` Rust tool for generating
  the `tests/run-test.sh` test suite runner.
- Python test suite:
  - add version constraints for all the Python library dependencies

### Other changes

- Python test suite:
  - drop the flake8 + hacking test environment
  - use the requirements.txt file in the test setuptools infrastructure
  - adapt the test programs to the new API of the cfg-diag library

## [0.2.1] - 2022-07-13

### Fixes

- Python test suite:
  - correct the package name in the internal metadata
- Documentation:
  - use "destination" instead of "hostspec" in the manual page and
    the program usage message

### Additions

- Documentation:
  - add some examples to the program usage message

### Other changes

- Python test suite:
  - use Python 3.7 deferred annotations
  - use `dict`, `list`, etc. instead of the `typing` generics
  - use `subprocess.run()` instead of a `Popen` object as appropriate
  - add cfg-diag 0.2.x as allowed dependency versions

## [0.2.0] - 2022-03-24

### Additions

- Allow options to be passed to the executed program.
- Add support for ssh://[username@]host[:port] URIs.
- Test suite improvements:
  - add a Python test suite running an SSH server
  - run the Python test suite in the Makefile's "test" target
  - add helper tools for running the Python test suite in a Docker
    container

### Other changes

- Code style improvements:
  - treat the $hostspec, $filename, and $opts variables as global
  - add helper functions for invoking the SSH client
  - add an EditorConfig definitions file
- Test suite improvements:
  - push the tests down into a tests/ directory tree

## [0.1.0] - 2021-12-15

### Started

- First public release.

[Unreleased]: https://gitlab.com/ppentchev/remrun/-/compare/release%2F0.2.4...main
[0.2.4]: https://gitlab.com/ppentchev/remrun/-/compare/release%2F0.2.3...release%2F0.2.4
[0.2.3]: https://gitlab.com/ppentchev/remrun/-/compare/release%2F0.2.2...release%2F0.2.3
[0.2.2]: https://gitlab.com/ppentchev/remrun/-/compare/release%2F0.2.1...release%2F0.2.2
[0.2.1]: https://gitlab.com/ppentchev/remrun/-/compare/release%2F0.2.0...release%2F0.2.1
[0.2.0]: https://gitlab.com/ppentchev/remrun/-/compare/release%2F0.1.0...release%2F0.2.0
[0.1.0]: https://gitlab.com/ppentchev/remrun/-/tags/release%2F0.1.0
