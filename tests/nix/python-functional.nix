# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

{ pkgs ? import <nixpkgs> { }
, py-ver ? "11"
}:
let
  python-name = "python3${py-ver}";
  python = builtins.getAttr python-name pkgs;
in
pkgs.mkShell {
  buildInputs = [
    pkgs.locale
    pkgs.openssh
    python
  ];
  shellHook = ''
    set -e
    PYTHONPATH="$(pwd)/python" python3.${py-ver} -B -u -m run_sshd_test -v -- ../remrun.sh
    PYTHONPATH="$(pwd)/python" python3.${py-ver} -B -u -m run_sshd_test -v -t "$(pwd)/run-test.sh" -- ../remrun.sh
    exit
  '';
}
