#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

: "${PY_MINVER_MIN:=10}"
: "${PY_MINVER_MAX:=14}"

for pyver in $(seq -- "$PY_MINVER_MIN" "$PY_MINVER_MAX"); do
	printf -- '\n===== Running tests for 3.%s\n\n\n' "$pyver"
	nix-shell --pure --argstr py-ver "$pyver" nix/python-functional.nix
	printf -- '\n===== Done with 3.%s\n\n' "$pyver"
done
