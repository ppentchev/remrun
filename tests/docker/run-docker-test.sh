#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

self="$(readlink -f -- "$0")"
dockerdir="$(dirname -- "$self")"
testdir="$(dirname -- "$dockerdir")"
srcdir="$(dirname -- "$testdir")"

tgtdir='/opt/remrun-src'
envdir='/opt/remrun-venv-run-sshd-test'

distro="${1:-bookworm}"

docker run --rm -it -v "$srcdir:$tgtdir:ro" -- "remrun/sshd:$distro" env "PYTHONPATH=$tgtdir/tests/python" "$envdir/bin/python3" -m run_sshd_test -v "$tgtdir/remrun.sh"

docker run --rm -it -v "$srcdir:$tgtdir:ro" -- "remrun/sshd:$distro" env "PYTHONPATH=$tgtdir/tests/python" "$envdir/bin/python3" -m run_sshd_test -t "$tgtdir/tests/run-test.sh" -v "$tgtdir/remrun.sh"

docker run --rm -it -v "$srcdir:$tgtdir:ro" -- "remrun/sshd:$distro" env "PYTHONPATH=$tgtdir/tests/python" "$envdir/bin/python3" -m run_sshd_test -v -u remruntest "$tgtdir/remrun.sh"

docker run --rm -it -v "$srcdir:$tgtdir:ro" -- "remrun/sshd:$distro" env "PYTHONPATH=$tgtdir/tests/python" "$envdir/bin/python3" -m run_sshd_test -t "$tgtdir/tests/run-test.sh" -v -u remruntest "$tgtdir/remrun.sh"
