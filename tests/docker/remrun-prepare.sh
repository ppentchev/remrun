#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

set -e

usage()
{
	cat <<'EOUSAGE'
Usage:	remrun-prepare.sh apt|yum /path/to/requirements.txt
EOUSAGE
}

if [ ! -f "$2" ]; then
	usage 1>&2
	exit 1
fi

case "$1" in
	apt)
		[ -n "$SKIP_APT_UPDATE" ] || apt-get update

		env DEBIAN_FRONTEND=noninteractive apt-get install -y python3 python3-venv openssh-client openssh-server
		python='python3'
		;;

	yum)
		python='python3.12'
		yum -y install "$python" openssh openssh-clients openssh-server
		;;

	*)
		usage 1>&2
		exit 1
		;;
esac

vdir="/opt/remrun-venv-run-sshd-test"
vpy="$vdir/bin/python3"

"$python" -m venv -- "$vdir"

installed="$("$vpy" -m pip list --format freeze | cut -d= -f1)"
# shellcheck disable=SC2086
"$vpy" -m pip install -U -- $installed

"$vpy" -m pip install -r "$2"
