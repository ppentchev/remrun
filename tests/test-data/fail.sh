#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

echo "I am failing, I am failing home again, 'cross the sea..."
exit 42
