#!/bin/sh
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

if [ "$#" -eq 0 ]; then
	echo 0
elif [ "$#" -eq 1 ]; then
	seq "$1" | tr -d '\n'
else
	echo 'Something weird going on here'
	exit 1
fi
