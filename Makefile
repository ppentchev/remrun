#!/usr/bin/make -f
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

PROG=		remrun
SRC=		${PROG}.sh
MAN1=		${PROG}.1
MAN1GZ=		${MAN1}.gz

TEST_TOOL=	tests/run-test.sh

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}
BINDIR?=	${PREFIX}/bin
SHAREDIR?=	${PREFIX}/share
MANDIR?=	${SHAREDIR}/man/man
MAN1DIR?=	${MANDIR}1

BINOWN?=	root
BINGRP?=	root
BINMODE?=	755

SHAREOWN?=	${BINOWN}
SHAREGRP?=	${BINGRP}
SHAREMODE?=	644

INSTALL_PROGRAM?=	install -o '${BINOWN}' -g '${BINGRP}' -m '${BINMODE}'
INSTALL_DATA?=		install -o '${SHAREOWN}' -g '${SHAREGRP}' -m '${SHAREMODE}'

MKDIR_P?=	mkdir -p

PYTHON3?=	python3

all:		${PROG} ${MAN1GZ}

${PROG}:	${SRC}
		install -m 755 -- '${SRC}' '${PROG}'

${MAN1GZ}:	${MAN1}
		gzip -c -n -9 -- '${MAN1}' > '${MAN1GZ}' || { rm -f -- '${MAN1GZ}'; false; }

test:		all
		[ -n '${SKIP_SHELLCHECK}' ] || shellcheck -- '${PROG}' '${TEST_TOOL}'
		env PYTHONPATH='${CURDIR}/tests/python' ${PYTHON3} -B -u -m run_sshd_test -v -t '${TEST_TOOL}' $(if ${TEST_USERNAME},-u '${TEST_USERNAME}') -- '${PROG}'

install:	all
		${MKDIR_P} '${DESTDIR}${BINDIR}'
		${INSTALL_PROGRAM} -- '${PROG}' '${DESTDIR}${BINDIR}/'

		${MKDIR_P} '${DESTDIR}${MAN1DIR}'
		${INSTALL_DATA} -- '${MAN1GZ}' '${DESTDIR}${MAN1DIR}/'

clean:
		rm -f -- '${PROG}' '${MAN1GZ}'

.PHONY:		all test clean
