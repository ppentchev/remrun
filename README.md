<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# remrun - copy a file over to a remote host and execute it

The `remrun` tool may be useful for one-off execution of a series of
commands or a complicated command that does not easily lend itself to
shell quoting and escaping via SSH.

## Usage

    remrun [-Nsv] [--noop] [--stdin] [--verbose] hostspec filename

The remrun utility copies the specified file to the remote host using SSH
under a temporary filename, executes it on the remote side, then removes
the temporary file. By default, the remote program does not have access to
the current process's standard input stream; the `-s` option has to be passed
explicitly to enable that.

As a special case, if "-" (a single dash) is passed as the `filename`
argument, `remrun` will read the contents of the standard input, store
it into a temporary file, and then copy and execute that file, removing it
afterwards. This is incompatible with the `-s` (`--stdin`) option.

The remote host to transfer the file to may be specified in two ways: either
as a `[username@]host` pattern, or as a `ssh://[username@]host[:port][/]`
URI.

Note that the `sha256sum` tool is used both locally and remotely to make
sure that the file has been transferred correctly. Its use on the local host
may be overridden by the `-C` command-line option, and on the remote host -
by the `-c` one.

## Examples

Copy the whoami.sh file over to the “server” host, logging in as the “jrl”
user account, then run it:

    remrun jrl@server whoami.sh

Do the same, but for a remote host that runs FreeBSD and another command
must be used instead of `sha256sum`:

    remrun -c 'sha256 -r' jrl@server whoami.sh

Do the same, but with some more verbose output and allowing the executed
program to read from our standard input stream:

    remrun -vs jrl@server whoami.sh

Specify the commands to run directly:

    printf 'uname -a\ndate -R\n' | remrun jrl@server -

## Comments

Please send all comments, problem reports, or suggestions to
[Peter Pentchev](mailto:roam@ringlet.net).
