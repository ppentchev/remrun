// SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
// SPDX-License-Identifier: BSD-2-Clause
//! Common definitions for the generate-tests tool.

use camino::{Utf8Path, Utf8PathBuf};
use serde::{Deserialize, Serialize};

/// Runtime configuration for the `generate_tests` tool.
#[derive(Debug)]
pub struct Config {
    /// The path to the shell program to generate.
    output_file: Utf8PathBuf,
}

impl Config {
    /// The path to the shell program to generate.
    #[inline]
    #[must_use]
    pub fn output_file(&self) -> &Utf8Path {
        &self.output_file
    }

    /// Construct a new [`Config`] object.
    #[inline]
    #[must_use]
    pub fn new(output_file: Utf8PathBuf) -> Self {
        Self { output_file }
    }
}

/// The description of a single test case.
#[derive(Debug, Deserialize, Serialize)]
pub struct TestCase {
    /// The human-readable name of the test case.
    title: String,

    /// Is this test supposed to succeed?
    success: bool,

    /// The name of the file to send/execute.
    filename: String,

    /// The regular expression to match the remrun output against.
    output_template: String,

    /// The options to pass to the program to execute.
    program_options: Vec<String>,

    /// The options to pass to remrun.
    remrun_options: Vec<String>,

    /// The name of the file to redirect the standard input from.
    stdin_filename: Option<String>,
}

impl TestCase {
    /// The human-readable name of the test case.
    #[inline]
    #[must_use]
    pub fn title(&self) -> &str {
        &self.title
    }

    /// Is this test supposed to succeed?
    #[inline]
    #[must_use]
    pub const fn success(&self) -> bool {
        self.success
    }

    /// The name of the file to send/execute.
    #[inline]
    #[must_use]
    pub fn filename(&self) -> &str {
        &self.filename
    }

    /// The regular expression to match the remrun output against.
    #[inline]
    #[must_use]
    pub fn output_template(&self) -> &str {
        &self.output_template
    }

    /// The options to pass to the program to execute.
    #[inline]
    #[must_use]
    pub fn program_options(&self) -> &[String] {
        &self.program_options
    }

    /// The options to pass to remrun.
    #[inline]
    #[must_use]
    pub fn remrun_options(&self) -> &[String] {
        &self.remrun_options
    }

    /// The name of the file to redirect the standard input from.
    #[inline]
    #[must_use]
    pub fn stdin_filename(&self) -> Option<&str> {
        self.stdin_filename.as_deref()
    }
}

/// The full test data.
#[derive(Debug, Deserialize, Serialize)]
pub struct TestData {
    /// The test cases themselves.
    tests: Vec<TestCase>,
}

impl TestData {
    /// The test cases themselves.
    #[inline]
    #[must_use]
    pub fn tests(&self) -> &[TestCase] {
        &self.tests
    }
}
